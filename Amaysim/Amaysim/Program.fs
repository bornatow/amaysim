﻿open System
open canopy
open canopy.runner
open canopy.types
open reporters

open OpenQA.Selenium
open OpenQA.Selenium.Firefox
open OpenQA.Selenium.Chrome

[<EntryPoint>]
let main args = 
    let environmentArg = args.[0].ToLower()
    let browserArg     = args.[1].ToLower()
    let testArg        = args.[2].ToLower()

    let path = System.AppDomain.CurrentDomain.BaseDirectory

    reporter <- new LiveHtmlReporter(Chrome, path) :> IReporter // remove these lines to stop html log
    let liveHtmlReporter = reporter :?> LiveHtmlReporter
    liveHtmlReporter.reportPath <- Some ("C:\\logs2\\log")

    match browserArg with
    | "chrome" ->   let chromePath = "ChromeDriver.exe"
                    let mutable chromeProfilePath = System.IO.Path.Combine(path, "ChromeProfile")
                    chromeDir <- if System.IO.File.Exists(chromePath) then path else @"C:\"
                    chromeProfilePath <- if System.IO.Directory.Exists(chromeProfilePath) then chromeProfilePath else @"C:\SELENIUMPROFILE"
                    System.Console.WriteLine("Using chrome profile in: " + chromeProfilePath)
                    let options = new ChromeOptions()
                    options.AddArguments([|"log-path=c:\log\chromedriver.log"; "verbose"; "test-type"; "--disable-extensions"; "--disable-cache"; "--incognito"; "user-data-dir=" + chromeProfilePath|]);
                    let chromeWithOptions = ChromeWithOptions options
                    start chromeWithOptions 
    | _ -> failwith "Invalid browser argument selected. Supports chrome only for the time being."

    pin FullScreen

    match testArg with
    | "my settings" -> mySettings.runner environmentArg
    | _ -> failwith "Test not implemented"

    try run()
    finally quit()

    0 // return an integer exit code
