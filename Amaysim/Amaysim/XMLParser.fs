﻿module XMLParser

open System.Xml
open System.Collections.Generic

let readData (test:string) xpath =
    let listResult = new List<string>()
    let doc = new XmlDocument() in
    doc.Load (@"..\..\..\DATA\"+test+".xml") // maybe a better way for future
    doc.SelectNodes xpath
        |> Seq.cast<XmlNode>
        |> Seq.map (fun node -> node.Value)
        |> Seq.iter (fun e -> listResult.Add(e))// |> ignore ) //String.concat "<>"
    listResult

let readFirst test xpath =
    try  (readData test xpath).[0]
    with | :? System.ArgumentException -> ""

let readAllSections test = (readData test "//section/@title")

let readParamForSection test section param = readFirst test ("//section[@title='" + section + "']//" + param + "/text()")

let paramExistsInSection test section param = (readFirst test ("//section[@title='" + section + "']//" + param + "/@status")) = "on"