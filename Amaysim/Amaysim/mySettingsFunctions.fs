﻿module mySettingsFunctions

open System.Collections.Generic
open canopy
open OpenQA.Selenium
open pageObjects
open commonFunctions
open XMLParser

let testName = "MySettings"

let sectionRow section = elements _rows |> List.find(fun c -> c.Text.StartsWith(section))

let sectionLocationY section = 
    let el = elements _rows |> List.find(fun c -> c.Text.StartsWith(section))
    el.Location.Y + el.Size.Height // saves row position on the page

let saveCurrentState section (checkbox:bool) =
    let mutable result = ""
    let currentValueEl = sectionRow section |> elementWithin (readParamForSection testName section "currentValueSel")
    if checkbox then result <- currentValueEl.Selected.ToString()
    else result <- currentValueEl.Text
    if paramExistsInSection testName section "additionalValueSel" then 
        let currentAdditionalValue = sectionRow section |> elementWithin (readParamForSection testName section "additionalValueSel")
        result <- result + ";" + currentAdditionalValue.Text
    result

let closeSuccessPopup _ = 
    waitUntilLoaded()
    waitUntilDisplayedSelector _timeout _popupSuccess
    //verify text message
    click (concatinateSelectors [_popupSuccess; _closePopup])

let changeAnyCheckbox section =
    let checkbox = sectionRow section |> elementWithin _checkbox
    let checkboxOriginalStatus = checkbox.Selected
    click (checkbox |> parent |> elementWithin "label") // this could have been handled by one line with click instead of specific chack/uncheck if it wasn't for the special case with International Roaming
    if (paramExistsInSection testName section "confirmationNeeded" && checkboxOriginalStatus = false) || section = "Usage alerts" then 
        waitUntilDisplayedSelector _timeout _confirmPopup
        waitUntilLoaded()
        click (concatinateSelectors [_confirmPopup; _confirmPopupYes])
    if paramExistsInSection testName section "successPopup" then closeSuccessPopup() else waitUntilLoaded()
    let checkboxStatusAfterChange = (sectionRow section |> elementWithin _checkbox).Selected
    if checkboxOriginalStatus = checkboxStatusAfterChange then raise (CanopyNotEqualsFailedException(sprintf "Change saved incorrectly. Start state: %b Current state: %b" checkboxOriginalStatus checkboxStatusAfterChange))
    else describe "Change confirmed"

let changeAnyEdit section =
    let mutable textFieldSel = _textField
    clickWithin (sectionRow section) _editLink
    if section = "Call forwarding" then // don't like this one
        textFieldSel <- _callForwardingNumber
        waitUntilLoaded()
        waitUntilDisplayedSelector _timeout _popup
        click _popupConfirm
        sleep 0.3 // animation explicit wait - not nice
        sectionRow section |> elementsWithin ".radio" |> List.find(fun c -> c.Text = "Yes") |> click // always selects YES - temp
    waitUntilLoaded()
    scrollToElement3 (sectionLocationY section) // after clicking Edit, page is scrolled to top

    if paramExistsInSection testName section "textField" then
        waitUntilDisplayedElement _timeout (sectionRow section |> elementWithin textFieldSel)
        let valueFormat = readFirst testName ("//section[@title='" + section + "']//" + "textField//accepting/text()")
        let valueMaxLength = readFirst testName ("//section[@title='" + section + "']//" + "textField//maxLength/text()")
        let testValue = randomizeValidValue valueFormat (int(valueMaxLength))
        (sectionRow section |> elementWithin textFieldSel) << testValue // insert text into the field
        clickWithin (sectionRow section) _confirmChanges
        //There is a bug with call forwarding, when the validation error is displayed, Save button cannot be clicked the first time
        if isDisplayed _confirmChanges && isNotDisplayed _loadingScreen then clickWithin (sectionRow section) _confirmChanges // bug workaround for call forwarding - temporary solution, will not work when more then 1 parameters remain open
        if paramExistsInSection testName section "successPopup" then closeSuccessPopup() else waitUntilLoaded()
        waitForT _timeout (fun _ -> (sectionRow section |> unreliableElementsWithin _editLink).[0].Text = "Edit") // wait for Edit link to appear again
        waitUntilLoaded()
        let valueAfterChange = (sectionRow section |> elementWithin (readParamForSection testName section "currentValueSel")).Text.Trim()
        if (valueAfterChange = testValue) || (section = "Call forwarding" && valueAfterChange.EndsWith(testValue)) then describe "Change confirmed"
        else raise (CanopyNotEqualsFailedException(sprintf "Change saved incorrectly. Expected: %s GOT: %s" testValue valueAfterChange))

let changeSettings section (sectionType:string) =
    match sectionType.ToLower() with
    | "checkbox" -> changeAnyCheckbox section
    | "edit"     -> changeAnyEdit section
    | "dropdown" | "link" 
                 -> describe "Not implemented"
    | "static"   -> describe "No action required for static rows"
    | _ -> raise (CanopyNotEqualsFailedException(sprintf "Section type %s not recognized" sectionType))

let mockComparisonAlgorithm (startState:Dictionary<string,string>) (endState:Dictionary<string,string>) (section:string) =
    describe (startState.[section]) // debug
    describe (endState.[section]) // debug
    let currentState = saveCurrentState section ((readParamForSection testName section "type") = "Checkbox")
    describe currentState // debug
    if endState.[section] <> currentState then 
        describe "The algorithm is not finished. Needs something more sophisticated than equality check. This is a simple failing condition"
    else describe "This clause passes the test"

let navigateToMenu menu =
    clickMenuOption menu 
    waitUntilDisplayedSelector _timeout _pageHeading
    waitForT _timeout (fun _ -> ((element _pageHeading).Text.Trim() = menu)) // Wait for Page heading to change to expected value