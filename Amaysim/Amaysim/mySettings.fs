﻿module mySettings

open System
open System.Collections.Generic
open canopy
open OpenQA.Selenium
open pageObjects
open commonFunctions
open mySettingsFunctions
open XMLParser


//the variables are global due to some F# limitations
let testTitle = "My Settings"
let mutable startState = new Dictionary<string,string>() // a global mutable variable to store current state of call forwarding
let mutable endState = new Dictionary<string,string>()
let mutable changedTo = new Dictionary<string,string>()

let runner env =
    let user = "0468827174"
    let pass = "theHof34"
    let sections = readAllSections testName
    context "Pre Test Log in and navigate to My Settings"
    "Log in as " + user &&& fun _ ->
        logIn env user pass
    "Navigate to My Settings" &&& fun _ ->
        navigateToMenu testTitle
    
    for s in 0..sections.Count-1 do
        context (sections.[s])
        let sectionType = readParamForSection testName sections.[s] "type"
        sections.[s] + " - save start setting value" &&& fun _ ->
            scrollToElement3 (sectionLocationY sections.[s]) // chrome driver needs to scroll to have the element displayed on the screen
            startState.Add(sections.[s],saveCurrentState sections.[s] (sectionType = "Checkbox"))
        sections.[s] + " - negative test for validator would come here for text fields where applicable" &&! fun _ -> // to be done &&! operator skips the test
            describe "To be done"
        sections.[s] + " - change setting value" &&& fun _ ->
            changeSettings sections.[s] sectionType
        sections.[s] + " - save end setting value" &&& fun _ ->
            scrollToElement3 (sectionLocationY sections.[s])
            endState.Add(sections.[s],saveCurrentState sections.[s] (sectionType = "Checkbox"))
        sections.[s] + " - confirm end setting value without logging out" &&& fun _ -> // to be done
            mockComparisonAlgorithm startState endState sections.[s]
    
    context "Verify data change after logging out and in again"
    "Log Out and Log In" &&& fun _ ->
        logout()
        logIn env user pass // one uncessary step in here
    "Navigate to My Settings" &&& fun _ ->
        navigateToMenu testTitle

    for s in 0..sections.Count-1 do
        sections.[s] + " - confirm end setting value after relogin" &&& fun _ -> // to be done
            mockComparisonAlgorithm startState endState sections.[s]

    //ADDITIONAL TESTS FOR CALL FORWARDING WOULD BE HERE - radio button, phone number formats, tests would probably include logging in and out several times as well

    context "Change all data back to the original values"
    "Not done" &&! fun _ ->
        describe "Not done"