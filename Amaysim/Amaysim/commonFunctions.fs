﻿module commonFunctions

open canopy
open pageObjects
open OpenQA.Selenium

let decr user = "theHoff34" // decripting function for passwords kept in hashed form - here always ""

let getUrl (env:string) =
    match env with
    | "production" -> "https://www.amaysim.com.au/my-account/my-amaysim/login"
    | _ -> raise (CanopyNotEqualsFailedException(sprintf "Environment %s not recognized" env))

let getPasswordForUser user = decr user

let waitForT = fun (timeout:float) f ->
    let originalTimeout = canopy.configuration.compareTimeout
    canopy.configuration.compareTimeout <- timeout
    try
        waitFor f
    finally
        canopy.configuration.compareTimeout <- originalTimeout

let isDisplayed (sel:string) = (unreliableElement sel).Displayed
let isNotDisplayed (sel:string) = not (isDisplayed sel)

let waitUntilDisplayedSelector timeout sel = waitForT timeout (fun _ -> (isDisplayed sel)) // implicit wait for an element to be displayed with timeout

let waitUntilDisplayedElement timeout (el:IWebElement) = waitForT timeout (fun _ -> (el.Displayed))

let waitUntilLoaded() = waitForT _timeout (fun _ -> isNotDisplayed _loadingScreen)

let logIn env user pass =
    url (getUrl env)
    waitUntilDisplayedSelector _timeout _password
    _login << user
    _password << getPasswordForUser user
    click _logInBtn
    waitUntilDisplayedSelector _timeout _simInfo

let clickMenuOption (option:string) =
    elements _menuOption
    |> List.find(fun c -> c.Text = option)
    |> click

let logout _ = 
    clickMenuOption "Logout"

let jsClick sel =
    js (sprintf "")

let scrollToElement sel = // the following two lines should work, but I cannot find a proper element to scroll, applying workaround to save time in developing the test
    let heightValue = (element sel).Location.Y
    js (sprintf "$('%s').scrollTop(%d)" _scrollableBody heightValue) |> ignore
   
let scrollToElement2 sel = // workaround for the upper function
    if (element sel).Location.Y > (element _scrollableBody).Size.Height then click _pageHeading; press Keys.PageDown

let scrollToElement3 (y:int) = // more useful for particular approach
    if y > (element _scrollableBody).Size.Height then click _pageHeading; press Keys.PageDown

let clickWithin (parentEl:IWebElement) childSel = parentEl |> elementWithin childSel |> click

let concatinateSelectors (selectors:string list) = String.concat " " [for s in selectors do yield s]

let randomizeValidValue (valueFormat:string) (valueMaxLength:int) =
    let chars = if valueFormat.ToLower() = "num" then "0123456789" else "ABCDEFGHIJKLMNOPQRSTUVWUXYZ0123456789qwertyuiopasdfghjklzxcvbnm" // no special chars for now @#$&()-_+{}[]
    let charsLen = chars.Length
    let random = System.Random()
    let mutable randomChars = [|for i in 1..valueMaxLength -> chars.[random.Next(charsLen)]|]
    if valueFormat.ToLower() = "num" && valueMaxLength=10 then 
        randomChars.[0] <- '0'
        if "01".Contains(randomChars.[1].ToString()) then randomChars.[1] <- '2' // to ensure number format, I hope
    new System.String(randomChars)