﻿module pageObjects

//global variables
let _timeout = 30.0

//Login page
let _login = "#my_amaysim2_user_session_login"
let _password = "#password"
let _logInBtn = "#login_button"

//My Settings
let _iconLogout = ".icon-logout"
let _loadingScreen = "#ajax_loading"
let _scrollableBody = "#body-content"
let _menuOption = ".ama-off-canvas-section-link"
let _rows = ".ama-list > .row"
let _popupBody = ".form_confirm_popup"
let _popup = ".form_confirm_popup"
let _confirmChanges = "input.button-green-action"
let _cancelChanges = ".grey" // use only within an object, otherwise may also click on Edit
let _popupConfirm = ".confirm_popup_confirm"
let _popupCancel = ".confirm_popup_cancel"
let _popupSuccess = ".form_info_popup.open"
let _closePopup = ".close-reveal-modal"
let _simInfo = ".sim-info"
let _pageHeading = ".ama-page-heading"
let _editLink = ".grey" // different name for different context
let _textField = "input.string"
let _checkbox = ".checkbox-no-text-content"
let _confirmPopup = ".confirm-modal.open"
let _confirmPopupYes = "#confirm_popup_yes"

let _editCallForwarding = "#edit_settings_call_forwarding"
let _updateCallForwardingForm = "#update_call_forwarding_form"
let _callForwardingNumber = "#my_amaysim2_setting_call_divert_number"
let _editAutoRecharge = "#edit_settings_auto_recharge"